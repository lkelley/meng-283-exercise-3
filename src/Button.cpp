#include "Button.h"
#include <Arduino.h>

Button::Button():
    _debounceTime(50), // Set 50ms debounce time
    _lastDebounce(0), // Init last debounce time
    _buttonState(LOW), // Init button state
    _lastButtonState(LOW) // Init last button state
{}


/**
 * Binds the button
 * 
 * @param pin Digital pin to bind the button to
 */
void Button::setPin(int pin) {
    _pin = pin;
}


/**
 * Gets the debounced state of the button
 * 
 * @return int Button state (HIGH or LOW)
 */
int Button::getState() {
    // Read the current pin state
    _buttonState = digitalRead(_pin);

    /*
    A different current state and previous state means the state
    has changed or the button is bouncing
     */
    if (_buttonState != _lastButtonState) {
        // Reset the last debounce clock
        _lastDebounce = millis();
        _lastButtonState = _buttonState;
    }

    // Debounce time has passed, return the current butto state
    else if (millis() - _lastDebounce > _debounceTime) {
        return _buttonState;
    }

    return LOW;
}
