/**
 * Simple class for controlling the state of an RGB LED
 */

#include <Arduino.h>
#include "LED.h"

/**
 * Constructor with pin binding
 */
LED::LED(short red, short green, short blue) {
    this->red = red;
    this->green = green;
    this->blue = blue;

    pinMode(this->red, OUTPUT);
    pinMode(this->green, OUTPUT);
    pinMode(this->blue, OUTPUT);
}

/**
 * Sets the colour of the LED
 */
void LED::setColour(short colour) {
    switch (colour) {
        case RED: {
            digitalWrite(this->red, LOW);
            digitalWrite(this->green, HIGH);
            digitalWrite(this->blue, HIGH);
            break;
        }

        case GREEN: {
            digitalWrite(this->red, HIGH);
            digitalWrite(this->green, LOW);
            digitalWrite(this->blue, HIGH);
            break;
        }

        case BLUE: {
            digitalWrite(this->red, HIGH);
            digitalWrite(this->green, HIGH);
            digitalWrite(this->blue, LOW);
            break;
        }

        default: {
            digitalWrite(this->red, HIGH);
            digitalWrite(this->green, HIGH);
            digitalWrite(this->blue, HIGH);
        }
    }
}