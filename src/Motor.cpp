/**
 * Simple motor controller class for use with delimited servo motors
 * 
 * After instantiation, set the inversion and duty cycle
 */

#include "Motor.h"
#include <Arduino.h>
#include <Servo.h>

/**
 * Standard constructor with no binding
 */
Motor::Motor() {
    this->setInverted(false);
}

/**
 * Binds the servo to the specified pin. Binding also sets the pin mode.
 */
void Motor::bind(short pin) {
    pinMode(pin, OUTPUT);
    this->servo.attach(pin);
}

/**
 * Sets inverted operation for left side motors
 */
void Motor::setInverted(bool inverted) {
    this->inverted = inverted;
}

/**
 * Sets the duty cycle of the motor, which affects speed
 */
void Motor::setDutyCycle(short dutyCycle) {
    this->dutyCycle = constrain(dutyCycle, 0, 100);
    this->tickPeriod = this->dutyCycle * this->dutyPeriod / 100;
}

/**
 * Sets the stop position (in microseconds PW) of the servo. This point must be found prior to use.
 */
void Motor::setStopPosition(unsigned short stopPosition) {
    this->stopPosition = stopPosition;
}

/**
 * Move motor forward (CW)
 */
void Motor::goForward() {
    this->direction = this->inverted ? BACKWARD : FORWARD;
    this->start();
}

/**
 * Move motor backward (CCW)
 */
void Motor::goBackward() {
    this->direction = this->inverted ? FORWARD : BACKWARD;
    this->start();
}

/**
 * Start motor operation
 */
void Motor::start() {
    this->run = true;
    this->startTime = micros();
}

/**
 * Stop motor operation
 */
void Motor::stop() {
    this->run = false;
}

/**
 * Motor clock function
 */
void Motor::update() {
    this->lastActionTime = micros();

    /*
    When running, the motor's speed is governed by a pseudo-PWM system
    */
    if (this->run) {
        // Determine if in the "TICK" period by modding the time in this action with the duty period
        if ((this->lastActionTime - this->startTime) % this->dutyPeriod <= this->tickPeriod) {
            this->servo.write(this->direction);
        } else { // TOCK period of PWM
            this->servo.writeMicroseconds(this->stopPosition);
        }
    } else { // Stop motor
        this->servo.writeMicroseconds(this->stopPosition);
    }
}