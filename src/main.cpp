#include <Arduino.h>
#include "Motor.h"
#include "LED.h"
#include "Button.h"

const short LEFT_MOTOR = 8;
const short RIGHT_MOTOR = 9;

const short LEFT_SENSOR = 11;
const short RIGHT_SENSOR = 10;

const short LEFT_LIGHT_RED = 4;
const short LEFT_LIGHT_GREEN = 5;
const short LEFT_LIGHT_BLUE = 6;

const short RIGHT_LIGHT_RED = 1;
const short RIGHT_LIGHT_GREEN = 2;
const short RIGHT_LIGHT_BLUE = 3;

// Motor vars
Motor leftMotor;
Motor rightMotor;

// Button vars
Button leftSensor;
Button rightSensor;

// LED vars
LED leftLED(LEFT_LIGHT_RED, LEFT_LIGHT_GREEN, LEFT_LIGHT_BLUE);
LED rightLED(RIGHT_LIGHT_RED, RIGHT_LIGHT_GREEN, RIGHT_LIGHT_BLUE);

// Timing vars
const unsigned long backupTime = 500;
const unsigned long avoidTime = 250;

// Forward declarations
void goForward();
void goBackward();
void stop();
void turnLeft();
void turnRight();


void setup() {
    // Set sensor pin modes
    pinMode(LEFT_SENSOR, INPUT_PULLUP);
    pinMode(RIGHT_SENSOR, INPUT_PULLUP);

    // Setup left motor
    leftMotor.bind(LEFT_MOTOR);
    leftMotor.setStopPosition(1500); // TODO: tune
    leftMotor.setDutyCycle(100);
    
    // Setup right motor
    rightMotor.bind(RIGHT_MOTOR);
    rightMotor.setInverted(true);
    rightMotor.setStopPosition(1500); // TODO: tune
    rightMotor.setDutyCycle(100);

    // Setup left sensor
    leftSensor.setPin(LEFT_SENSOR);

    // Setup right sensor
    rightSensor.setPin(RIGHT_SENSOR);

    // Flash pin 13, 1 Hz, 5 times
    pinMode(13, OUTPUT);
    for (int i = 1; i <= 5; i++) {
        digitalWrite(13, HIGH);
        delay(500);
        digitalWrite(13, LOW);
        delay(500);
    }
}


void loop() {
    // Start by going forward
    goForward();

    if (leftSensor.getState() == HIGH && rightSensor.getState() == HIGH) {
        // Wait, non-blocking
        return;
    }

    // Collision has happened

    stop();

    goBackward();

    if (leftSensor.getState() == LOW) { // Collision on left
        turnRight();
    } else { // Collision on right
        turnLeft();
    }
}


void goForward() {
    leftLED.setColour(GREEN);
    rightLED.setColour(GREEN);

    leftMotor.goForward();
    rightMotor.goForward();
}


void goBackward() {
    leftLED.setColour(RED);
    rightLED.setColour(RED);
    
    leftMotor.goBackward();
    rightMotor.goBackward();
    
    delay(backupTime);

    stop();
}


void stop() {
    leftLED.setColour(OFF);
    rightLED.setColour(OFF);

    leftMotor.stop();
    rightMotor.stop();
}


void turnLeft() {
    leftLED.setColour(BLUE);
    rightLED.setColour(OFF);

    leftMotor.goBackward();
    rightMotor.goForward();

    delay(avoidTime);

    stop();
}


void turnRight() {
    leftLED.setColour(OFF);
    rightLED.setColour(BLUE);

    leftMotor.goForward();
    rightMotor.goBackward();

    delay(avoidTime);

    stop();
}
