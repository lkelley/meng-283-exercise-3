#define OFF -1
#define RED 0
#define GREEN 1
#define BLUE 2

class LED {
    private:
        short red;
        short green;
        short blue;
    
    public:
        LED(short red, short green, short blue);
        void setColour(short);
};