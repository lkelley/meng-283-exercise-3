/**
 * Simple motor controller class for use with delimited servo motors
 */

#include <Arduino.h>
#include <Servo.h>

#define FORWARD 180
#define BACKWARD 0

class Motor
{
    private:
        Servo servo;

        bool inverted; // Inverted operation reverses the forward and backward directions

        unsigned short dutyCycle;
        unsigned short tickPeriod;
        const unsigned short dutyPeriod = 1000;

        unsigned long startTime;
        unsigned long lastActionTime;
        
        unsigned short direction;
        unsigned short stopPosition;
        bool run;

    public:
        Motor();

        void bind(short);
        void setInverted(bool);
        void setDutyCycle(short);
        void setStopPosition(unsigned short);

        void start();
        void stop();

        void goForward();
        void goBackward();
        
        void update();
};
